// selectors/users.js

import { get } from 'lodash';

function getUser({ users }, userId) {
  return users.get(userId);
}

/**
 * Inject a user into the props based on configurable prop names
 */
export function namedUserPipeline(propIn, propOut) {
  /**
   * @param {object} state - Redux state (connect).
   * @param {string} props.propIn - Which prop to use for the userId lookup.
   * @param {string} props.propOut - Which prop to use for the user injection.
   */
  return withReduxState({
    injectProps: {
      [propOut]: (state, props) => getUser(state, get(props, propIn)),
    },
    injectEffects: {
      updateUser: 'users.updateUser',
    },
  });
}

export const userFromUserId = namedUserPipeline('userId', 'user');
