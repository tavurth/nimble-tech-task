/**
 * CREATE a redux state selector, which includes updater functions as required.
 *
 * @param {object} options - Options to select from the redux state.
 * @returns {Pipeline} A react-redux connect pipeline.
 */
export function withReduxState(options = {}) {
  const { injectProps = {}, injectEffects = {} } = options;

  return connect(
    // Inject the props asked for by running the selector on the
    // current redux state, and also props.stageId
    (state, props) => {
      // Allow the user to pass a functional prop resolver
      if (injectProps instanceof Function) {
        return injectProps(state, props);
      }

      const toReturn = {};

      Object.entries(injectProps).forEach(([propName, selectorFunc]) => {
        toReturn[propName] = selectorFunc(state, props);
      });

      return toReturn;
    },

    // We'll also inject an object of all the updater functions requested (if any)
    (dispatch, props) => {
      // Here we'll test the users passed values
      // to determine which type of function we should resolved
      const resolveDispatcher = maybeFunction => {
        // Value is already the function we don't do anything
        if (maybeFunction instanceof Function) {
          return maybeFunction;
        }

        // Will try to fetch the function from stores dispatch
        return get(dispatch, maybeFunction, noop);
      };

      // Allow the user to pass a functional effect resolver
      if (injectEffects instanceof Function) {
        return injectEffects(dispatch, props);
      }

      const toReturn = {};

      Object.entries(injectEffects).forEach(([name, updaterFuncName]) => {
        toReturn[name] = resolveDispatcher(updaterFuncName);
      });

      return toReturn;
    },
  );
}
