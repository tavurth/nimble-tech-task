// container.js
function Container({ userAId, userBId }) {
  return (
    <Fragment>
      <ChildComponent userId={userAId} />
      <ChildComponent userId={userBId} />
    </Fragment>
  );
}

export default Container;
