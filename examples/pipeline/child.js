// child.js
function ChildComponent({ user }) {
  return <span>{user.name}</span>;
}

ChildComponent.propTypes = {
  userId: PropTypes.string.isRequired,

  // provided by userFromUserId
  user: PropTypes.shape({ name: PropTypes.string }).isRequired,
};

export default userFromUserId(ChildComponent);
