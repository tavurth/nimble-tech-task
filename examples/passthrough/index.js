function ChildComponent({ user }) {
  return <span>{user.name}</span>;
}

function ReduxContainer({ userA, userB }) {
  return (
    <Fragment>
      <ChildComponent user={userA} />
      <ChildComponent user={userB} />
    </Fragment>
  );
}

export default connect(
  // mapStateToProps
  ({ users }, { userAId, userBId }) => ({
    userA: users.get(userAId),
    userB: users.get(userBId),
  }),
)(ReduxContainer);
