- [Nimble](#sec-1)
  - [Propose](#sec-1-1)
  - [Targets](#sec-1-2)
  - [Notes](#sec-1-3)
  - [Inventory checklist](#sec-1-4)
  - [Views and routing](#sec-1-5)
  - [Styled components](#sec-1-6)
  - [Testing](#sec-1-7)
  - [Priorities](#sec-1-8)
    - [Stage 1](#sec-1-8-1)
    - [Stage 2](#sec-1-8-2)
    - [Stage 3](#sec-1-8-3)
  - [Redux - Our state management tool](#sec-1-9)
    - [Redux containers](#sec-1-9-1)
    - [Store structure](#sec-1-9-2)
    - [`stations`](#sec-1-9-3)
    - [`routes`](#sec-1-9-4)
    - [`journeys`](#sec-1-9-5)
    - [`settings`](#sec-1-9-6)
    - [`passengers`](#sec-1-9-7)
  - [Components](#sec-1-10)
    - [Search](#sec-1-10-1)
    - [Tickets](#sec-1-10-2)
    - [Passengers](#sec-1-10-3)
    - [Notification](#sec-1-10-4)
    - [Payment](#sec-1-10-5)
    - [Confirmation](#sec-1-10-6)

# Nimble<a id="sec-1"></a>

Lead Software Developer (Web)

## Propose<a id="sec-1-1"></a>

1.  an architecture overview for the front-end of an isomorphic web application (UI file) and
2.  a product backlog plan with priorities and order of implementation.

For the latter, we would like to see what you would prioritise to develop first, second, third… and last.

## Targets<a id="sec-1-2"></a>

-   Chrome \* / Chrome mobile (iOS/Android) \*
-   Firefox \* / Firefox mobile (iOS/Android) \*
-   Safari / Safari mobile
-   IE11 / Edge and above

## Notes<a id="sec-1-3"></a>

-   It’s a real world application that was built for the German and Austrian markets 🤓
-   It’s a single page application doing one single thing: buying transport tickets.
-   The flow starts by the search then each card appears after each step is completed.
-   It’s possible for the user to scroll back up thus invalidating the cards shown afterwards.

## Inventory checklist<a id="sec-1-4"></a>

The architecture details should cover the following:

-   Inventory of Redux containers (or alternative)
-   Inventory of React JS / Vue JS Components
-   Structure of the Redux store
-   Management of stylesheets and views
-   Automated tests strategy (libraries, coverage)

## Views and routing<a id="sec-1-5"></a>

We'll primarily use `react-router` and `react-router-redux` for our internal SPA routing however at lower levels (inside container components) we'll use a modified recompose branch for simple logical splitting of components.

Please see the following for reference:

```js
//Components/SomethingComplex/index.js
import Loader from './Loader'
import ErrorComponent from './Error'
import ComponentA from './ComponentA'
import ComponentB from './ComponentB'

function isLoading({ loading }) {
  return loading;
}

function isErrored({ error }) {
  return error;
}

function isGroup({ data }) {
  return data instanceof Array;
}

export default compose(
  // Conditionally show the loader
  branch(isLoading)(Loader),

  // Conditionally show the error component
  branch(isErrored)(ErrorComponent),

  // Conditionally show the group component
  branch(isGroup)(ComponentA),
)(ComponentB)
```

## Styled components<a id="sec-1-6"></a>

Previously I've used `scss` extensively and much enjoyed it's descriptive power,

however due to a simplification of process for all involved I'd like to propose `styled components` for this SPA.

Styled components are in my opinion superior for this task as they promote composition over generation, which leads to better readabiltity.

An example:

```js
// utils/styles
export function highlightOnError({ error }) {
  if (error) {
    return 'background: red;'
  }
}
```

```js
import { highlightOnError } from 'utils/styles'

const MyStyledComponent = `
  ${highlightOnError}

  // ...additionalStyles
`
```

Advantages:

-   Composition of functional styles (see above)
-   Promotes the creation of modified component layers
-   Better support for [SSR rendered buffer styles](https://github.com/styled-components/styled-components-website/blob/master/sections/advanced/server-side-rendering.md)

Downsides to the approach are:

-   Parent-child composition can be more difficult,
-   For newer developers it can be a little confusing
-   Certain tooling breaks when using styled components (prettier not always working)
-   Older versions cause issues with isomorphic design (style dependency calculation)

Since this application is supposed to be SSRendered, we would do well to choose the more recent v2+ of styled components which supports style dependency extraction

## Testing<a id="sec-1-7"></a>

We'll be using jest for our testing coverage.

We'll aim to cover 60% of the project in unit tests, primarily logic heavy components and the redux stores.

We'll then cover logic heavy components, specifically those using branching render pipelines.

TDD is preffered if developers are familiar, and a TDD framework can be constructed in the first stages of the project. (See order of work)

TDD Framework use would look something akin to the following:

```javascript
// someTest.spec.js
import testBuilder from 'utils/testing';
import { routeSelector } from 'selectors/routes';

export default testBuilder([
  {
    name: 'Sets loading state correctly',
    setup: ({ spyOn, injectProp }) => {
      const routeId = 'f00f121d-5001-4c4f-96c6-8c5b48f78fff';

      spyOn({
        // Keep track of this route at each stage of the test
        routeToWatch: curryRight(routeSelector)(routeId), // We'll pass state here from spyOn
      });

      // Inject our routeId to each action (see below)
      inject({ routeId });
    },
    actions: [
      ({ dispatch }) => {
        dispatch.routes.reset();
      },
      ({ dispatch, routeId }) => {
        // We've shimmed the action to return immediately in testing
        dispatch.routes.loadOne(routeId);
      },
      ({ routeToWatch }) => {
        expect(routeToWatch).toBeInstanceOf(Object);
        expect(routeToWatch.loading).toEqual(true);
      },
    ],
  },
]);
```

## Priorities<a id="sec-1-8"></a>

My initial priorities would be to get a simplified wireframe up and running as soon as possible, then slowly moving to pad and cover that wireframe with real data.

Let's look at a sample team:

-   Lead **(L)**
-   Backend **(B)**
-   Frontend **(F)**
-   Designer **(D)**
-   Product Owner **(P)**

By this time we've thankfully used **(D)** to their full potential, realising the scope and detailed design aspects of the task.

### Stage 1<a id="sec-1-8-1"></a>

**(P)** Groom all aspects of the plan which are uncertain

**(B)** will be working on the initial data packet, and the endpoints to resolve an id to each of the data types. Ideally backend would follow a similar data structure to the frontend.

**(B)** will also be required to work on the construction of the graph search algorithm if one is not provided

**(L)** Will focus on the construction of the Redux store and any associated components.

**(L)** Will start to build out a set of very basic wireframes, in communication with **(F)** to understand which would be the most reusable.

**(F)** Will work on a storyboard set of basic components, perhaps using React-Storyboard

### Stage 2<a id="sec-1-8-2"></a>

**(B)** Implements payment gateway. **(B)** IDP is now finished and frontend can resolve data.

**(L)** Redux state is now complete, focus on state transitions and validation components

**(F)** Switch from construction of raw storyboard components to implementation of those components on the wirefwrame built in step 1

### Stage 3<a id="sec-1-8-3"></a>

**(B)** Continues work on route search algorithm optimization

**(L)** Focus on polishing and refining store architecture, such items as autoclean

**(F)** Focus on remaining implementation of UI and polish

## Redux - Our state management tool<a id="sec-1-9"></a>

### Redux containers<a id="sec-1-9-1"></a>

At many times in my career I've seen redux containers slow an application when they are used too high in the react node tree.

Because of this I'd like to propose that connecting components at a lower level to be a more robust and error free process for the injection of state.

Where we might normally see something like this:

```javascript
function ChildComponent({ user }) {
  return <span>{user.name}</span>;
}

function ReduxContainer({ userA, userB }) {
  return (
    <Fragment>
      <ChildComponent user={userA} />
      <ChildComponent user={userB} />
    </Fragment>
  );
}

export default connect(
  // mapStateToProps
  ({ users }, { userAId, userBId }) => ({
    userA: users.get(userAId),
    userB: users.get(userBId),
  }),
)(ReduxContainer);
```

I'd prefer to see something akin to the following:

```javascript
// child.js
function ChildComponent({ user }) {
  return <span>{user.name}</span>;
}

ChildComponent.propTypes = {
  userId: PropTypes.string.isRequired,

  // provided by userFromUserId
  user: PropTypes.shape({ name: PropTypes.string }).isRequired,
};

export default userFromUserId(ChildComponent);
```

```javascript
// container.js
function Container({ userAId, userBId }) {
  return (
    <Fragment>
      <ChildComponent userId={userAId} />
      <ChildComponent userId={userBId} />
    </Fragment>
  );
}

export default Container;
```

Where our helper function `userFromUserId` can be found in the [here](./examples/pipeline/userSelectors.js) alongside it's helper [withReduxState](./examples/pipeline/withReduxState.js).

The advantage of connecting state at as low a level as possible are numerous:

-   Reduction of render thrashing of the parent components
-   No render of sibling component `userB` if `userA` is updated
-   Component can depend on only a `userId` making it more composable
-   Component can be `export` ed from the file directly allowing alternate user injection

Downsides to the approach are:

-   Must use memoized selectors as without doing so may cause a drop in processing speed as we call multiple `mapStateToProps`.
-   HOCs are less familiar, and a more magic to newcomers, although adoption is quick
-   HOCs may add render overhead (CPU) if badly built, and will always add a small memory overhead

To finalize the above I'd like to point out that injection of state is a more complex programming logic and should be centralised. Use of selectors and selector helpers such as `withReduxState` allow everyday use of complex logic without any complex thought requirement.

### Store structure<a id="sec-1-9-2"></a>

```javascript
export default {
  routes,
  journeys,
  stations,

  settings,
  payments,

  passengers,
};
```

1.  Propagation of data

    Backend data is propagated to frontend on request.
    
    Backend propagates primarily arrays of Ids.
    
    Ids are resolved to normalized data points in the associated store.

2.  Autoclean

    Stores are automatically cleaned up using an autoclean interval
    
    Every `2,000 actions` or so we'll dispatch a process which can remove stale data
    
    Purpose primarily to keep memory footprint in check
    
    `Journies` are used to keep track of last updated data If not updated frequently we'll remove their stack of data recursively through each store
    
    **NOTE**: This could be difficult if we don't reference count each station or route. Perhaps we should look into doing a reference count sweep once in a while (lazy) Or building a generic store for `lastUpdated` by itemId (runtime cheap)

### `stations`<a id="sec-1-9-3"></a>

```js
const Station = PropTypes.shape({
  id: PropTypes.uuid.isRequired,
  name: PropTypes.string.isRequired,
  linkedTo: PropTypes.shape({}) // TODO: Groom backend
})
```

1.  Actions

    -   `setStation`
    
    Stations will be primarily updated from backend data, so we need few here

### `routes`<a id="sec-1-9-4"></a>

-   Should contain route results
-   Route results are fetched by ID from restful or ws API

```js
const Route = PropTypes.shape({
  start: PropTypes.date,
  finish: PropTypes.date,
  id: PropTypes.uuid.isRequired,
  type: PropTypes.string.isRequired, // Type of route "Train"
  price: PropTypes.string.isRequired, // TODO: Groom currency
  stations: PropTypes.arrayOf(PropTypes.stationId).isRequird, // uuid => station.id
})
```

1.  Actions

    -   `setRoute`
    
    Routes will be primarily updated from backend data, so we need few here

### `journeys`<a id="sec-1-9-5"></a>

```js
const Journey = PropTypes.shape({
  id: PropTypes.id.isRequird,
  start: PropTypes.uuid.isRequird, // First station
  finish: PropTypes.uuid.isRequird, // Last station
  class: PropTypes.string.isRequired, // "Second class"
  lastUpdated: PropTypes.date.isRequird, // For autoclean
  routes: PropTypes.arrayOf(PropTypes.route), // uuid => route.id
  repeatCount: PropTypes.number.isRequird, // Prioritisation in offline search
  passengers: PropTypes.arrayOf(PropTypes.passengerId), // uuid => passenger.id
})
```

-   Persist to local storage
-   Recent journeys searched by user

1.  Actions

    -   `addJourney`
    -   `updateJourney`
    -   `incRepeatCount`
    -   `setLastUpdated`
    -   `setJourneyStart`
    -   `setJourneyClass`
    -   `setJourneyRoutes` Primarily backend
    -   `setJourneyFinish`
    -   `addJourneyPassenger`
    -   `removeJourneyPassenger`

### `settings`<a id="sec-1-9-6"></a>

```js
const settings = PropTypes.shape({
  discounts: PropTypes.shape({
    [userAgeType]: PropTypes.float // ({ "14 Jahre": 0.75 }) Young people pay less
  }),
  journey: PropTypes.journeyId, // uuid => journey.id (current journey)
  id: PropTypes.date.isRequird,
  info: PropTypes.shape({
    email: PropTypes.string,
    dob: PropTypes.string.isRequird,
    last: PropTypes.string.isRequird,
    first: PropTypes.string.isRequird,
  })
})
```

1.  Actions:

    -   `setInfo` Overwrites any info present
    -   `setJourney` Set a new uuid to track the current journey
    -   `updateInfo` Patches info with changes

### `passengers`<a id="sec-1-9-7"></a>

-   **NOTE**: Groom `GDPR`
-   Previously input passenger information.

```js
const Passenger = PropTypes.shape({
  id: PropTypes.date.isRequird,
  age: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
})
```

1.  Actions

    -   `addPassenger`
    -   `updatePassenger`
    -   `removePassenger`

## Components<a id="sec-1-10"></a>

<a id="org0ec3180"></a>

### Search<a id="sec-1-10-1"></a>

Search component provides a simple state handler State handler performs duties such as:

-   Toggle route types
-   Save search result route Ids

1.  Initial page

    1.  Start & Final station
    
        **GROOM** OptionA
        
        1.  Posts current string query to backend.
        2.  Backend resolves to an array of stationIds
        3.  StationIds are resolved from redux or auto-fetched
        4.  Stations are displayed by name
        
        **GROOM** OptionB
        
        1.  Posts current string query to backend.
        2.  Backend resolves to an array of stations
        3.  Stations are normalized into the station store
        4.  Stations are displayed by name
        
        OptionB seems to be the better solution in this case as this will likely be our users first touch of the application.
        
        The solution fails where we have offline first, and we would still like to display common route names.
        
        When these stations are then selected as our route start and end points. We perform another second backend search for routes.
        
        Backend can then resolve us either the full Route object (deep including stations) or a shallow route object (routeIds).
        
        I'd prefer a deep Route object here as we're still in the early stages, and route searching is a fairly difficult task. Fetching the station set from the db should be little overhead compared to the request complexity.
    
    2.  Date selector
    
        Simple date selector Saves selected date to the current `redux.settings.journeyId` (uuid dereference)

2.  Header

    1.  Type checkbox
    
        Works in tandem with the route selector (See Start & Final station)
        
        Saved to the stateful component, see [State handler performs duties](#org0ec3180)
    
    2.  Filter button
    
        -   Modal
        -   Header
        -   Filter radio
        -   Filter section dropdown
        
        Types of transportation are available as part of the Initial data package Saved to the stateful component, see [State handler performs duties](#org0ec3180)

3.  Journey list

    This page is the result of the users stateful search criteria (see [1.10](#org0ec3180)) Each journey is then a combination of routes, with each route being a combination of stations
    
    -   Ticket price
    -   Ticket details button
    -   Reservations dropdown
    
    1.  Route composition
    
        -   Route type

4.  No routes found page

### Tickets<a id="sec-1-10-2"></a>

Tickets composes data from the passenger table, and the route table for prices We'll use the `redux.settings.discounts` with each passenger age to calculate the price

1.  Route section detail

    1.  Route type header
    
        -   Icon
    
    2.  Route section body
    
        -   Route colors (Base on route type)
        -   Route stations
        -   Route types
        -   Route price
        -   Buy ticket checkbox
        
        **GROOM**: Client thinks that people should pay €17 to walk 10 minutes ![img](./walking.png)

2.  Footer

    -   Total price of selected routes
    -   Buy tickets button

### Passengers<a id="sec-1-10-3"></a>

1.  Passenger list

    Can simply iterate over all available `passengerId` in `journey.passengers` `passengerId` will then be connected to `Passenger` inside `PassengerBlock`
    
    1.  Passenger block
    
        Inject update passengerById action
        
        -   Age selector
        -   Name input with icon
        -   Class selector
        -   Add person button
        -   Delete person button

2.  Footer

    -   Total price of selected routes
    -   Buy tickets button (See tickets)

### Notification<a id="sec-1-10-4"></a>

-   Email input with label
-   Send email button
-   Ticket link (**GROOM**: should this scroll back up?)

### Payment<a id="sec-1-10-5"></a>

-   Header icon

1.  Credit card info

    -   Number with card type icon (input mask)
    -   Date selector (input mask)
    -   CVC (input mask)
    
    -   Backend connection
    -   We need validation here
    -   **GROOM** Which payment gateway are we going to use
    -   **GROOM** Will backend implement payment gateway

2.  Right panel

    -   Text with email address
    -   Total price (See Tickets)
    -   Buy tickets button

### Confirmation<a id="sec-1-10-6"></a>

1.  Header

    -   Icon
    -   User email from notification step
    -   Ticket download link

2.  Related offers table

    -   Standardised logo and name
    -   Book by partner button
